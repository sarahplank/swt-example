# Rostocker Nachbarschaftshilfe

## General

This project aims to support local...

## Documentation

### Backend

#### Used technologies

- Docker
- PostgreSQL
- Spring Boot
- Hibernate
- Java 17

#### Development setup

##### Prerequisites

- Setup a PostgreSQL DBMS via Docker
    - Install Docker for Windows
    - Run in a Command Prompt
    ```cmd
    docker run --name swt -p 5432:5432 -d -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=swt postgres:14
    ```
    - Check if the Docker container is running
    ```cmd
    docker ps -a
    ```
    - *Optional:* Stop the Docker container
    ```cmd
    docker stop swt
    ```
    - *Optional:* Start the Docker container
    ```cmd
    docker start swt
    ```

- Setup DBeaver, an SQL client to connect to the PostgreSQL DBMS Docker container
    - Install DBeaver
    - Click `Add a PostgreSQL DBMS in DBeaver`
    - Fill in the connection information
        - `address`: `localhost:5432`
        - `username`: `postgres`
        - `password`: `postgres`
    - Switch to the `PostgreSQL` tab and click on `Show all databases`
    - Click `Connect`
    - On the left you can expand the tree view by clicking on `Postgres`, `Databases` and browse the `swt` database 

- Install Eclipse
- Install Postman
- Install Lombok

##### Project setup

- Run in a Command Prompt
```cmd
gradlew eclipse
```
- Open Eclipse
- Import as a Gradle Project

- Open Postman
- Import the Postman collection

### Frontend
