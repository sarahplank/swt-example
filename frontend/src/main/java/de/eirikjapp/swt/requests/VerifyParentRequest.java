package de.eirikjapp.swt.requests;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import com.google.gson.Gson;

import de.eirikjapp.swt.models.UserSearchModel;
import de.eirikjapp.swt.models.UserVerificationModel;

public class VerifyParentRequest {
	public UserVerificationModel sendRequest(long parentId) throws IOException, InterruptedException {
		Gson gsonObject = new Gson();
		
		HttpClient client = HttpClient.newHttpClient();

		HttpRequest request = HttpRequest.newBuilder().uri(URI.create("http://localhost:8080/users/verifyParent/"+parentId))
				.GET()
				.header("Content-Type", "application/json").build();

		HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString()); 
		String responseString = response.body(); //convertiere response zu string
		UserVerificationModel parentVerification = new Gson().fromJson(responseString, UserVerificationModel.class);
		System.out.println(responseString);
		return parentVerification;
	}
}
