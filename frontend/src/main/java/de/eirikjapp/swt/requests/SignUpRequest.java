package de.eirikjapp.swt.requests;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import com.google.gson.Gson;

import de.eirikjapp.swt.models.UserModel;
import de.eirikjapp.swt.models.UserSearchModel;

public class SignUpRequest {
	public UserSearchModel sendRequest(UserModel user) throws IOException, InterruptedException {
		Gson gsonObject = new Gson();
		
		HttpClient client = HttpClient.newHttpClient();

		HttpRequest request = HttpRequest.newBuilder().uri(URI.create("http://localhost:8080/users"))
				.PUT(HttpRequest.BodyPublishers.ofString(gsonObject.toJson(user)))
				.header("Content-Type", "application/json").build();

		HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString()); 
		String responseString = response.body(); //convertiere response zu string
		UserSearchModel generatedUser = new Gson().fromJson(responseString, UserSearchModel.class);
		System.out.println(responseString);
		return generatedUser;
		
	}
}
