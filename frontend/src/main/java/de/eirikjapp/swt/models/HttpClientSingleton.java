package de.eirikjapp.swt.models;

import java.net.http.HttpClient;

import lombok.Getter;
import lombok.Setter;

/**
 * This singleton is for reusing the same HttpClient over and over again without
 * the need to create a new one. In this way the Session Cookies are saved and
 * can be used between requests.
 */
@Setter
@Getter
public class HttpClientSingleton {

    private static HttpClientSingleton httpClientSingleton = new HttpClientSingleton();

    private HttpClient httpClient = HttpClient.newHttpClient();

    public static HttpClientSingleton HttpClientSingleton() {
        return httpClientSingleton;
    }
}
