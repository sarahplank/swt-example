package de.eirikjapp.swt.models;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class NewsletterDTO {
	private Long id;

	private String title;

	private String text;

	private String link;
	
    private LocationCategoryModel locationCategory;
	
	private UserModel user;
	
    private String dateTime;
	
    private List<NewsletterCategoryModel> newsletterCategories;
}
