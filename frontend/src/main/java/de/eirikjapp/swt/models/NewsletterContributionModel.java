package de.eirikjapp.swt.models;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class NewsletterContributionModel {

	private Long id;

	private String title;

	private String text;

	private String link;
	
	private LocationCategoryModel location;
	
	private UserModel user;
	
	private List<NewsletterCategoryModel> categoryModels;
	
	//private UserModel user;
	
    //private List<CategoryModel> categoryModels;
}
