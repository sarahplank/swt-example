package de.eirikjapp.swt.models;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class UserModel {

    private Long id;

    private String firstName;

    private String lastName;
    
    private String passwort;
    
    private String email;
    
    private Role role;
    
    private Set<UserModel> parents;
    
    //private Set<UserModel> childIds;
}