package de.eirikjapp.swt.models;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class LocationCategoryModel {

    private Long id;

    private String location;

    private Set<NewsletterContributionModel> newsletterContributionModel;
}

