package de.eirikjapp.swt.models;

public enum Role {
	Child, UserOfFullAge, Admin
}
