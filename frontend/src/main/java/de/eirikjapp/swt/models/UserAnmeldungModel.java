package de.eirikjapp.swt.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class UserAnmeldungModel {
    private Long id;
    
    private String passwort;
}
