package de.eirikjapp.swt.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ParentVerificationModel {
	private Boolean verification;
	private Long id;
}
