package de.eirikjapp.swt.models;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserIdSingleton {

    private static UserIdSingleton userIdSingleton = new UserIdSingleton();

    private Long userId = 0l;

    public static UserIdSingleton UserIdSingleton() {
        return userIdSingleton;
	}
}
