package de.eirikjapp.swtfrontend;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SwtFrontendApplicationImpl extends Application {
	@Override
	public void start(Stage stage) {
		try {
			Parent root =  FXMLLoader.load(getClass().getResource("Anmeldung.fxml"));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setResizable(false);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void inition(String[] args) {
		launch(args);
	}
}
