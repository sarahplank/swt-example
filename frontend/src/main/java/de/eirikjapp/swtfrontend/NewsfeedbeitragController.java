package de.eirikjapp.swtfrontend;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.controlsfx.control.CheckComboBox;

import com.google.gson.Gson;

import de.eirikjapp.swt.models.AllLocationCategoryModel;
import de.eirikjapp.swt.models.AllNewsletterCategoryModel;
import de.eirikjapp.swt.models.HttpClientSingleton;
import de.eirikjapp.swt.models.LocationCategoryModel;
import de.eirikjapp.swt.models.NewsletterCategoryModel;
import de.eirikjapp.swt.models.NewsletterContributionModel;
import de.eirikjapp.swt.models.NewsletterDTO;
import de.eirikjapp.swt.models.UserIdSingleton;
import de.eirikjapp.swt.models.UserModel;
import de.eirikjapp.swt.models.UserVerificationModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;

public class NewsfeedbeitragController implements Initializable {

	@FXML
	TextField titel;
	@FXML
	TextArea beitragstext;

	@FXML
	ChoiceBox<String> locationFilterBox;
	@FXML
	CheckComboBox<String> checkComboBoxCategory;

	// @FXML Button zz; // bild button, noch inaktiv momentan
	@FXML
	Button newsfeedbeitragErstellenButton;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
       
		//get NewsletterCategoryModels
		Gson gson = new Gson();

		HttpClient client = HttpClientSingleton.HttpClientSingleton().getHttpClient();
		
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create("http://localhost:8080/newsletterCategories/all"))
                .GET()
				.header("Content-Type", "application/json")
				.build();

		HttpResponse<String> response = null;
		try {
			response = client.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		AllNewsletterCategoryModel allNewsletterCategoryModel = gson.fromJson(response.body(), AllNewsletterCategoryModel.class);
        
        //get LocationCategoryModels
        Gson gson2 = new Gson();
		
		HttpRequest request2 = HttpRequest.newBuilder().uri(URI.create("http://localhost:8080/locationCategories/all"))
                .GET()
				.header("Content-Type", "application/json")
				.build();

		HttpResponse<String> response2 = null;
		try {
			response2 = client.send(request2, HttpResponse.BodyHandlers.ofString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        AllLocationCategoryModel allLocationCategoryModel = gson2.fromJson(response2.body(), AllLocationCategoryModel.class);
        
        for(LocationCategoryModel locationCategoryModel : allLocationCategoryModel.getLocationCategoryModels()) {
        	String location = locationCategoryModel.getLocation();
        	locationFilterBox.getItems().add(location);
        }
        
        
        for(NewsletterCategoryModel newsletterCategoryModel : allNewsletterCategoryModel.getNewsletterCategoryModels()) {
        	String categoryName = newsletterCategoryModel.getCategoryName();
        	checkComboBoxCategory.getItems().add(categoryName);
        }
        
        /*
		ortFilterBox.getItems().addAll(ort);
		ortFilterBox.setOnAction(this::newsfeedbeitragErstellen);
		kategorieFilterBox1.getItems().addAll(kategorie);
		kategorieFilterBox1.setOnAction(this::newsfeedbeitragErstellen);
		kategorieFilterBox2.getItems().addAll(kategorie);
		kategorieFilterBox2.setOnAction(this::newsfeedbeitragErstellen); */
	}

	public void newsfeedbeitragErstellen(ActionEvent e) { // Newsfeed Beitrag erstellen
		ObservableList<String> checkedCategories = checkComboBoxCategory.getCheckModel().getCheckedItems();
		String selectedLocationCategory = locationFilterBox.getSelectionModel().getSelectedItem();
		
		NewsletterDTO newsletterDTO = new NewsletterDTO();
		String dateTime = LocalDateTime.now().toString();
		
		newsletterDTO.setDateTime(dateTime);
		
		// newsletterDTO.setLink(); fehlt in View

		Gson gson = new Gson();
       
		HttpClient client = HttpClientSingleton.HttpClientSingleton().getHttpClient();
		
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create("http://localhost:8080/locationCategories/findByName/"+selectedLocationCategory))
                .GET()
				.header("Content-Type", "application/json")
				.build();
		HttpResponse<String> response = null;
		try {
			response = client.send(request, HttpResponse.BodyHandlers.ofString());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		LocationCategoryModel locationCategoryModel = gson.fromJson(response.body(), LocationCategoryModel.class);
		
		newsletterDTO.setLocationCategory(locationCategoryModel);

		//newsletterDTO.setText(); noch Abfragen
		//newsletterDTO.setTitle(title); noch Abfragen
		Long singletonUserId = UserIdSingleton.UserIdSingleton().getUserId();
		
		UserModel userModel = new UserModel();
		
		userModel.setId(singletonUserId);
		
		newsletterDTO.setUser(userModel);
		

		List<NewsletterCategoryModel> checkedNewsletterCategories = new ArrayList<NewsletterCategoryModel>();
		for(String categoryName : checkedCategories) {
			Gson gson2 = new Gson();
			HttpRequest request2 = HttpRequest.newBuilder().uri(URI.create("http://localhost:8080/newsletterCategories/findByName/"+categoryName))
	                .GET()
					.header("Content-Type", "application/json")
					.build();
			HttpResponse<String> response2 = null;
			try {
				response2 = client.send(request2, HttpResponse.BodyHandlers.ofString());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			NewsletterCategoryModel newsletterCategoryModel = gson2.fromJson(response2.body(), NewsletterCategoryModel.class);
			checkedNewsletterCategories.add(newsletterCategoryModel);
		}
		
		newsletterDTO.setNewsletterCategories(checkedNewsletterCategories);
		
		//________________________________________________________________
		
		Gson gson3 = new Gson();
		
		HttpRequest request3 = HttpRequest.newBuilder().uri(URI.create("http://localhost:8080/newsletterContributions"))
                .PUT(HttpRequest.BodyPublishers.ofString(gson3.toJson(newsletterDTO)))
				.header("Content-Type", "application/json")
				.build();

		HttpResponse<String> response3 = null;
		try {
			response3 = client.send(request3, HttpResponse.BodyHandlers.ofString());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		NewsletterContributionModel allNewsletterCategoryModel = gson.fromJson(response3.body(), NewsletterContributionModel.class);
		System.out.println();
		
	}

}
