package de.eirikjapp.swtfrontend;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import com.google.gson.Gson;

import de.eirikjapp.swt.models.HttpClientSingleton;
import de.eirikjapp.swt.models.UserAnmeldungModel;
import de.eirikjapp.swt.models.UserIdSingleton;
import de.eirikjapp.swt.models.UserModel;
import de.eirikjapp.swt.models.UserVerificationModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AnmeldungController {

	@FXML
	private Label fehler;
	@FXML
	private TextField idText;
	@FXML
	private PasswordField passwortText;
	@FXML
	private Button confirmButton;
	@FXML
	private Button registrationButton;

	private Stage stage;
	private Scene scene;
	private Parent root;

	long anmeldeId;
	String anmeldePasswort;

	//status der anmeldung -> session noch zu implementieren


	public void switchToScene1(ActionEvent event) throws Exception {
		root = FXMLLoader.load(getClass().getResource("Anmeldung.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	public void vergessen(ActionEvent event) throws Exception {
		root = FXMLLoader.load(getClass().getResource("AnmeldungPV.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	public void submit(ActionEvent e) throws Exception {
		//Abfrage existens von id-passwort an DB response positiv -> Anmeldung, sonst Fehlermeldung
		//if-else
		anmeldeId = Long.parseLong(idText.getText());
		anmeldePasswort = passwortText.getText();

		UserAnmeldungModel userAnmeldungModel = new UserAnmeldungModel();
		userAnmeldungModel.setId(anmeldeId);
		userAnmeldungModel.setPasswort(anmeldePasswort);


        Gson gson = new Gson();

        HttpClient client = HttpClientSingleton.HttpClientSingleton().getHttpClient();

        UserIdSingleton singletonSessionId = UserIdSingleton.UserIdSingleton();
		
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create("http://localhost:8080/users/verifyLogin"))
                .POST(HttpRequest.BodyPublishers.ofString(gson.toJson(userAnmeldungModel)))
				.header("Content-Type", "application/json")
				.build();

		HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        UserVerificationModel verification = gson.fromJson(response.body(), UserVerificationModel.class);
        singletonSessionId.setUserId(verification.getId());
//---------------------------------------------
		//Mattes: wei� nicht genau was hier passieren soll

        /*
		UserModel userChildModel = new UserModel();
		userChildModel.setChildId(anmeldeId);

		userChildModel.setPasswort(anmeldePasswort);


		Gson gsonChildObject = new Gson();

		HttpClient childClient = HttpClient.newHttpClient();

		HttpRequest childRequest = HttpRequest.newBuilder().uri(URI.create("http://localhost:8080/users"))
				.GET()//aenderung userModel
				.header("Cookie", "SESSION=" + sessionId)
				.header("Content-Type", "application/json").build();

		HttpResponse<String> childResponse = client.send(request, HttpResponse.BodyHandlers.ofString());
		
         */


		root = FXMLLoader.load(getClass().getResource("Main.fxml"));
		stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
		//session id gemerkt

	}

	public void registration(ActionEvent e) throws Exception {
		root = FXMLLoader.load(getClass().getResource("Birthdate.fxml"));
		stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	/*
	 * try { Eingaben korrekt Wechsel zur Startseite }
	 *
	 * catch (Account ID nicht in Datenbank) { fehler.
	 * setText("Diese Account-ID existiert nicht. Bitte �berpr�fen Sie Ihre Eingabe."
	 * ); }
	 *
	 * catch (Passwort inkorrekt) { fehler.
	 * setText("Das Passwort ist nicht korrekt. Bitte �berpr�fen Sie Ihre Eingabe");
	 * } }
	 */

}