package de.eirikjapp.swtfrontend;

import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.stage.*;

import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;

import com.google.gson.Gson;

import de.eirikjapp.swt.models.HttpClientSingleton;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

public class MainController implements Initializable {

	/*
	 * Kommunikation & Label changing public void example(ActionEvent event) { int
	 * example; try { example = Integer.parseInt(myTextField.getText());
	 *
	 * if (example >= 18) { myLabel.setText("You are now signed up!"); } else {
	 * myLabel.setText("You must be 18+"); } } catch (Exception e) {
	 * myLabel.setText("error"); } }
	 */

	@FXML
	private Tab tabAnzeige;
	@FXML
	private Tab tabNewsfeed;
	@FXML
	private Tab tabProfil;
	@FXML
	private Button filterNewsfeedButton;
	@FXML
	private Button logoutButton;
	@FXML
	private ChoiceBox<String> ortsfilterBox;
	@FXML
	private ComboBox<String> kategoriefilterBox1;
	@FXML
	private ComboBox<String> kategoriefilterBox2;

	// ---------- Newsfeed FilterBoxen--------------------
	private String[] ort = { "S�dstadt", "KTV", "Stadtmitte", "Brinckmansdorf", "Biestow", "Gartenstadt",
			"Hansaviertel", "Reutershagen", "Evershagen", "L�tten Klein", "Lichtenhagen", "Schmarl", "Gro� Klein",
			"Gehlsdorf", "Dierkow", "Toitenwinkel" };
	private String[] kategorie = { "Verkehr", "Umwelt", "Kultur", "Sport", "Politik", "Gesellschaft", "Schn�ppchen",
			"Sonstiges" };

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		ortsfilterBox.getItems().addAll(ort);
		ortsfilterBox.setOnAction(this::applyNewsfeedFilter);
		kategoriefilterBox1.getItems().addAll(kategorie);
		kategoriefilterBox1.setOnAction(this::applyNewsfeedFilter);
		kategoriefilterBox2.getItems().addAll(kategorie);
		kategoriefilterBox2.setOnAction(this::applyNewsfeedFilter);

	}

	public void applyNewsfeedFilter(ActionEvent e) { // Newsfeed Filter anwenden

	}

	// ---------- Newsfeed FlowPane--------------------
	@FXML
	private FlowPane newsfeedFlowPane;
	@FXML
	private Pane newsfeedPane;
	@FXML
	private ImageView profilbildIcon;
	@FXML
	private Label newsfeedbeitragTitel;
	@FXML
	private Pane newsfeedbeitragTextPane;
	@FXML
	private Label newsfeedbeitragText;
	@FXML
	private Label newsfeedbeitragAutor;
	@FXML
	private Label ortAngabe;
	@FXML
	private Label newsfeedFilter1;
	@FXML
	private Label newsfeedFilter2;
	@FXML
	private Label newsfeedbeitragErstellungsdatum;
	@FXML
	private Button bild1Button;
	@FXML
	private Button bild2Button;
	@FXML
	private Button bild3Button;
	@FXML
	private Button bild4Button;
	@FXML
	private Button meldeButton;
	@FXML
	private Button newsfeedbeitragErstellungsButton;

	// -----------------------------------------------

	private Stage stage;
	private Scene scene;
	private Parent root;

	public void meldeBeitrag(ActionEvent e) throws Exception { // einen Beitrag melden
		try {
			Stage meldeStage = new Stage();
			Parent meldeRoot = FXMLLoader.load(getClass().getResource("Melden.fxml"));
			Scene meldeScene = new Scene(meldeRoot);
			meldeStage.setScene(meldeScene);
			meldeStage.show();

		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}

	public void newsfeedbeitragErstellen(ActionEvent e) throws Exception {
		try {
			Stage erstellNFStage = new Stage();
			Parent erstellNFRoot = FXMLLoader.load(getClass().getResource("Newsfeedbeitrag.fxml"));
			Scene erstellNFScene = new Scene(erstellNFRoot);
			erstellNFStage.setScene(erstellNFScene);
			erstellNFStage.show();

		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	public void getNewsfeedBild(ActionEvent e) { // einen Beitrag melden

	}

	public void logout(ActionEvent e) throws Exception { // abmelden
		Gson gson = new Gson();
		HttpClient client = HttpClientSingleton.HttpClientSingleton().getHttpClient();
		HttpRequest request = HttpRequest.newBuilder().uri(URI.create("http://localhost:8080/users/logout"))
				.GET()
				.header("Content-Type", "application/json")
				.build();
		
		HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Abmelden");
		alert.setHeaderText("Sie sind im Begriff, sich abzumelden!");
		alert.setContentText("Wollen sie sich wirklich abmelden?");

		if (alert.showAndWait().get() == ButtonType.OK) {
			root = FXMLLoader.load(getClass().getResource("Anmeldung.fxml"));
			stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();

		}
	}
}