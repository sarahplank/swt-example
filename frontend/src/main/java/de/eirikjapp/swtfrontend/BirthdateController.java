package de.eirikjapp.swtfrontend;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class BirthdateController {

	@FXML
	private TextField IDAge;
	@FXML
	private Button back;
	@FXML
	private Button ok;

	private Stage stage;
	private Scene scene;

	int age;

	public void submit(ActionEvent event) {
		try {
			age = Integer.parseInt(IDAge.getText());

			if (age >= 18) {
				Parent root = FXMLLoader.load(getClass().getResource("SignUp.fxml"));
				stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				stage.setScene(scene);
				stage.show();
			} else if (age >= 14 && age <= 18) {
				Parent root = FXMLLoader.load(getClass().getResource("SignUpChild.fxml"));
				stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
				scene = new Scene(root);
				stage.setScene(scene);
				stage.show();
			} else {
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Fehlermeldung");
				alert.setContentText("Du musst �ber 14 Jahre sein!");
				alert.showAndWait();
			}
		} catch (NumberFormatException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Fehlermeldung");
			alert.setContentText("Bitte nur Nummern eingeben!");
			alert.showAndWait();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void back(ActionEvent event) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("Anmeldung.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}
}