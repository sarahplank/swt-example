package de.eirikjapp.swtfrontend;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import de.eirikjapp.swt.models.UserModel;
import de.eirikjapp.swt.models.UserSearchModel;
import de.eirikjapp.swt.models.UserVerificationModel;
import de.eirikjapp.swt.requests.SignUpRequest;
import de.eirikjapp.swt.requests.VerifyParentRequest;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class SignUpChildController {

	@FXML
	private TextField IDVorname;
	@FXML
	private TextField IDNachname;
	@FXML
	private TextField IDEmailadresse;
	@FXML
	private PasswordField IDPasswort;
	@FXML
	private PasswordField IDPasswortAgain;
	@FXML
	private TextField IDParent1;
	@FXML
	private TextField IDParent2;
	@FXML
	private TextArea IDBeschreibung;
	@FXML
	private Button IDBestaetigung;
	@FXML
	private Button IDback;
	@FXML
	private Button addBildButton;

	private Stage stage;
	private Scene scene;

	String Vorname;
	String Nachname;
	String Email;
	String Passwort1;
	String Passwort2;
	Long IDEltern1;
	Long IDEltern2;
	String Profilbeschreibung;

	public void submit(ActionEvent event) throws IOException, InterruptedException {

		Vorname = IDVorname.getText();
		Nachname = IDNachname.getText();
		Email = IDEmailadresse.getText();
		Passwort1 = IDPasswort.getText();
		Passwort2 = IDPasswortAgain.getText();
		IDEltern1 = Long.parseLong(IDParent1.getText());
		IDEltern2 = Long.parseLong(IDParent2.getText());

		Profilbeschreibung = IDBeschreibung.getText();
		if (IDVorname.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Fehlermeldung");
			alert.setContentText("Bitte gib deinen Vornamen ein!");
			alert.showAndWait();
		} else if (IDNachname.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Fehlermeldung");
			alert.setContentText("Bitte gib deinen Nachnamen ein!");
			alert.showAndWait();
		} else if (IDEmailadresse.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Fehlermeldung");
			alert.setContentText("Bitte gib deine Emailadresse ein!");
			alert.showAndWait();
		} else if (IDPasswort.getText().length() <= 8) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Fehlermeldung");
			alert.setContentText("Dein Password ist zu kurz!");
			alert.showAndWait();
		} else if (!Passwort2.equals(Passwort1)) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Fehlermeldung");
			alert.setContentText("Deine Passw�rter stimmen nicht �berein !");
			alert.showAndWait();
		}

		if (IDParent1.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Fehlermeldung");
			alert.setContentText("Bitte gib die ID eines Elternteils ein!");
			alert.showAndWait();
		} else {
			
			UserModel userModel = new UserModel();
            userModel.setFirstName(Vorname);
            userModel.setLastName(Nachname);
            userModel.setPasswort(Passwort1);
            userModel.setEmail(Email);

            VerifyParentRequest verifyParentRequest1 = new VerifyParentRequest();
            UserVerificationModel userVerificationModel1 = verifyParentRequest1.sendRequest(IDEltern1);
            

            VerifyParentRequest verifyParentRequest2 = new VerifyParentRequest();
            UserVerificationModel userVerificationModel2 = verifyParentRequest2.sendRequest(IDEltern2);
            
            Set<UserModel> parents = new HashSet<>();
            if(userVerificationModel1.getResult() == true) {
            	UserModel parentModel1 = new UserModel();
            	parentModel1.setId(userVerificationModel1.getId());
            	parents.add(parentModel1);
            } else {
            	
            }
            
            if(userVerificationModel2.getResult() == true) {
            	UserModel parentModel2 = new UserModel();
            	parentModel2.setId(userVerificationModel2.getId());
            	parents.add(parentModel2);
            } else {
            	
            }
            
            userModel.setParents(parents);
            
            UserSearchModel generatedUser = new UserSearchModel();

            SignUpRequest request = new SignUpRequest();

            generatedUser = request.sendRequest(userModel);
			
			System.out.println(generatedUser);
			
			
			Parent root = FXMLLoader.load(getClass().getResource("Birthdate.fxml"));
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		}
	}

	public void back(ActionEvent event) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("Anmeldung.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}
}
