package de.eirikjapp.swtfrontend;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AnmeldungPWController {

	@FXML
	private Label fehler;
	@FXML
	private TextField myTextField1;
	@FXML
	private TextField myTextField2;
	@FXML
	private Button myButton1;
	@FXML
	private Button myButton2;

	private Stage stage;
	private Scene scene;
	private Parent root;

	public void switchToScene1(ActionEvent event) throws Exception {
		root = FXMLLoader.load(getClass().getResource("Anmeldung.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	public void switchToScene2(ActionEvent event) throws Exception {
		root = FXMLLoader.load(getClass().getResource("AnmeldungPV.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}

	public void submit(ActionEvent e) {
	}
	/*
	 * try { Eingaben korrekt
	 * fehler.setText("Das Passwort wurde an Ihre E-Mail-Adresse gesendet.");
	 * Wechsel zur Anmeldung } catch (Passwort inkorrekt) { fehler.
	 * setText("Das Passwort ist nicht korrekt. Bitte überprüfen Sie Ihre Eingabe");
	 * } }
	 */
}
