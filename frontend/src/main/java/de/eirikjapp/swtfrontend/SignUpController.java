package de.eirikjapp.swtfrontend;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import com.google.gson.Gson;

import de.eirikjapp.swt.models.UserModel;
import de.eirikjapp.swt.models.UserSearchModel;
import de.eirikjapp.swt.requests.SignUpRequest;
/*import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import org.controlsfx.control.NotificationPane;
import org.controlsfx.control.Notifications; */
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class SignUpController {

	@FXML
	private TextField IDVerein;
	@FXML
	private TextField IDVorname;
	@FXML
	private TextField IDNachname;
	@FXML
	private TextField IDEmailadresse;
	@FXML
	private PasswordField IDPasswort;
	@FXML
	private PasswordField IDPasswortAgain;
	@FXML
	private TextArea IDBeschreibung;
	@FXML
    private Button Bestaetigen;
	@FXML
	private Button back;
	@FXML
	private Button addBildButton;
	@FXML
	// *private NotificationPane notificationPane1;

	String Verein;
	String Vorname;
	String Nachname;
	String Email;
	String Passwort1;
	String Passwort2;
	String Profilbeschreibung;
	private Stage stage;
	private Scene scene;
	private Parent root;

	public void addBild(ActionEvent e) throws Exception {

	}

	public void submit(ActionEvent event) throws IOException, InterruptedException {
		Verein = IDVerein.getText();

		Vorname = IDVorname.getText();

		Nachname = IDNachname.getText();

		Email = IDEmailadresse.getText();

		Passwort1 = IDPasswort.getText();

		Passwort2 = IDPasswortAgain.getText();

		Profilbeschreibung = IDBeschreibung.getText();
		if (IDVorname.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Fehlermeldung");
			alert.setContentText("Bitte gib deinen Vornamen ein!");
			alert.showAndWait();
		} else if (IDNachname.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Fehlermeldung");
			alert.setContentText("Bitte gib deinen Nachnamen ein!");
			alert.showAndWait();

		} else if (IDEmailadresse.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Fehlermeldung");
			alert.setContentText("Bitte gib deine Emailadresse ein!");
			alert.showAndWait();
		} else if (IDPasswort.getText().length() <= 7) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Fehlermeldung");
			alert.setContentText("Dein Password ist zu kurz!");
			alert.showAndWait();
		} else if (!Passwort2.equals(Passwort1)) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Fehlermeldung");
			alert.setContentText("Deine Passwoerter stimmen nicht ueberein !");
			alert.showAndWait();
		}

		else {
			
			UserModel userModel = new UserModel();
			userModel.setFirstName(Vorname);
			userModel.setLastName(Nachname);
			userModel.setPasswort(Passwort1);
			userModel.setEmail(Email);
			UserSearchModel generatedUser = new UserSearchModel();
			
			SignUpRequest request = new SignUpRequest();
			
			generatedUser = request.sendRequest(userModel);
			
			// submit

			Parent root = FXMLLoader.load(getClass().getResource("Anmeldung.fxml"));
			stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			scene = new Scene(root);
			stage.setScene(scene);
			stage.show();

			// switch of scenes

			System.out.println(Vorname + " " + Nachname);

		}

		/*
		 * Notifications notficationBuilder =
		 * Notifications.create().title("Passwort stimmt nicht ?berein")
		 *
		 * .text("�berpr�fen Sie bitte Ihre Eingabe").graphic(null).hideAfter(Duration.
		 * seconds(10))
		 *
		 * .position(Pos.TOP_RIGHT).onAction(new EventHandler<ActionEvent>() {
		 *
		 * @Override
		 *
		 * public void handle(ActionEvent event) {
		 *
		 * System.out.println("Clicked on Notification");
		 *
		 * }
		 *
		 * });
		 */

		// passwort stimmt nicht ueberein

		// System.out.println("Error Passwort");

		// notficationBuilder.show();

	}

	public void back(ActionEvent event) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("Anmeldung.fxml"));
		stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
		scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

	}
}