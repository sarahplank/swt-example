package de.eirikjapp.swt;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import de.eirikjapp.swt.entities.LocationCategoryEntity;
import de.eirikjapp.swt.entities.NewsletterCategoryEntity;
import de.eirikjapp.swt.entities.NewsletterContributionEntity;
import de.eirikjapp.swt.entities.UserEntity;
import de.eirikjapp.swt.models.Role;
import de.eirikjapp.swt.repositories.LocationCategoryRepository;
import de.eirikjapp.swt.repositories.NewsletterCategoryRepository;
import de.eirikjapp.swt.repositories.NewsletterContributionRepository;
import de.eirikjapp.swt.repositories.UserRepository;

@SpringBootApplication
public class SwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwtApplication.class, args);
    }
    
    @Bean
    public CommandLineRunner initializeNewsletterCategoryData(NewsletterCategoryRepository repository) {
        return (args) -> {
            repository.save(new NewsletterCategoryEntity(1l, "Verkehr", null));
            repository.save(new NewsletterCategoryEntity(2l, "Umwelt", null));
            repository.save(new NewsletterCategoryEntity(3l, "Kultur", null));
            repository.save(new NewsletterCategoryEntity(4l, "Sport", null));
            repository.save(new NewsletterCategoryEntity(5l, "Politik", null));
            repository.save(new NewsletterCategoryEntity(6l, "Gesellschaft", null));
            repository.save(new NewsletterCategoryEntity(7l, "Schnaeppchen", null));
            repository.save(new NewsletterCategoryEntity(8l, "Sonstiges", null));
        };
    }
    
    @Bean
    public CommandLineRunner initializeUserData(UserRepository repository) {
        return (args) -> {
            repository.save(new UserEntity(1l, "Mattes", "Japp", "ericCantona<3", "testMail1", "#Link1", "IFC", null, Role.Child, null));
            repository.save(new UserEntity(2l, "Eirik", "Japp", "DogsAreCute", "testMail2", "#Link2", "Leguan", null, Role.UserOfFullAge, null));
            repository.save(new UserEntity(3l, "Tony Lennard", "Holz", "CatsAreCute", "testMail3", "#Link3", null, null, Role.Admin, null));
            repository.save(new UserEntity(4l, "Sarah", "Plank", "TurtlesAreCute", "testMail4", "#Link4", null, null, Role.Child, null));
            repository.save(new UserEntity(5l, "Hajiahmad", "Ahmadov", "PinguinsAreCute", "testMail5", "#Link5", "Uni Rostock", null, Role.UserOfFullAge, null));
            repository.save(new UserEntity(6l, "Fiona", "Stuerzekarn", "BirdsAreCute", "testMail6", "#Link6", null, null, Role.Admin, null));
            repository.save(new UserEntity(7l, "Finn", "Doose-Bruns", "FishAreCute", "testMail7", "#Link7", null, null, Role.Child, null));
            repository.save(new UserEntity(8l, "Vivian", "O'Dell", "SnakesAreCute", "testMail8", "#Link8", "Gartenverein Oberwarnow", null, Role.UserOfFullAge, null));
            repository.save(new UserEntity(9l, "Jennifer", "Krestel", "KirkDikDiksAreCute", "testMail9", "#Link9", "Handball Warnemuende", null, Role.Admin, null));
        };
    }

    @Bean
    public CommandLineRunner initializeLocationCategoryData(LocationCategoryRepository repository) {
        return (args) -> {
            repository.save(new LocationCategoryEntity(1l, "Südstadt", null));
            repository.save(new LocationCategoryEntity(2l, "KTV", null));
            repository.save(new LocationCategoryEntity(3l, "Stadtmitte", null));
            repository.save(new LocationCategoryEntity(4l, "Brinckmansdorf", null));
            repository.save(new LocationCategoryEntity(5l, "Biestow", null));
            repository.save(new LocationCategoryEntity(6l, "Gartenstadt", null));
            repository.save(new LocationCategoryEntity(7l, "Hansaviertel", null));
            repository.save(new LocationCategoryEntity(8l, "Reutershagen", null));
            repository.save(new LocationCategoryEntity(9l, "Evershagen", null));
            repository.save(new LocationCategoryEntity(10l, "Lütten Klein", null));
            repository.save(new LocationCategoryEntity(11l, "Lichtenhagen", null));
            repository.save(new LocationCategoryEntity(12l, "Schmarl", null));
            repository.save(new LocationCategoryEntity(13l, "Gross Klein", null));
            repository.save(new LocationCategoryEntity(14l, "Gehlsdorf", null));
            repository.save(new LocationCategoryEntity(15l, "Dierkow", null));
            repository.save(new LocationCategoryEntity(16l, "Toitenwinkel", null));
        };
    }

    @Bean
    public CommandLineRunner initializeNewsletterContributionData(NewsletterContributionRepository repository) {
        return (args) -> {
            NewsletterContributionEntity newsletterContribution = new NewsletterContributionEntity();
            
            // User - One-To-Many
            newsletterContribution.setUser(new UserEntity(1l, null, null, null, null, null, null, null, null, null));

            // LocationCategory - One-To-Many
            newsletterContribution.setLocationCategory(new LocationCategoryEntity(1l, null, null));

            // NewsletterCategory - Many-To-Many
            newsletterContribution.getNewsletterCategories().add(new NewsletterCategoryEntity(1l, null, null));

            repository.save(newsletterContribution);
        };
    }
}

