package de.eirikjapp.swt.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.eirikjapp.swt.entities.NewsletterContributionEntity;

@Repository
public interface NewsletterContributionRepository extends JpaRepository<NewsletterContributionEntity, Long> {
	//@Query("SELECT u FROM UserEntity u WHERE u.firstName = ?1 AND u.lastName = ?2")
	//Collection<UserEntity> findByName(String firstName, String lastName);
	
	//@Query("SELECT u FROM UserEntity u WHERE u.firstName = 'Mattes'")
	//Collection<UserEntity> findAllMattesUsers();
}
