package de.eirikjapp.swt.repositories;



import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import de.eirikjapp.swt.entities.NewsletterCategoryEntity;
import de.eirikjapp.swt.entities.UserEntity;

@Repository
public interface NewsletterCategoryRepository extends JpaRepository<NewsletterCategoryEntity, Long> {
	@Query("SELECT u FROM NewsletterCategoryEntity u WHERE u.name = ?1")
	NewsletterCategoryEntity findByName(String name);
	
	//@Query("SELECT u FROM UserEntity u WHERE u.firstName = ?1 AND u.lastName = ?2")
	//Collection<UserEntity> findByName(String firstName, String lastName);
	
	//@Query("SELECT u FROM UserEntity u WHERE u.firstName = 'Mattes'")
	//Collection<UserEntity> findAllMattesUsers();
}
