package de.eirikjapp.swt.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import de.eirikjapp.swt.entities.LocationCategoryEntity;

@Repository
public interface LocationCategoryRepository extends JpaRepository<LocationCategoryEntity, Long> {
	@Query("SELECT u FROM LocationCategoryEntity u WHERE u.location = ?1")
	LocationCategoryEntity findByName(String name);
	
    // @Query("SELECT u FROM UserEntity u WHERE u.firstName = ?1 AND u.lastName =
    // ?2")
    // Collection<UserEntity> findByName(String firstName, String lastName);

    // @Query("SELECT u FROM UserEntity u WHERE u.firstName = 'Mattes'")
    // Collection<UserEntity> findAllMattesUsers();
}
