package de.eirikjapp.swt.repositories;

import java.util.Collection;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import de.eirikjapp.swt.entities.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
	@Query("SELECT u FROM UserEntity u WHERE u.firstName = ?1 AND u.lastName = ?2")
	Collection<UserEntity> findByName(String firstName, String lastName);
	
	@Query("SELECT u FROM UserEntity u WHERE u.id = ?1 AND u.passwort = ?2")
	Collection<UserEntity> findByIdAndPassword(long id, String passwort);
	
	@Query("SELECT u FROM UserEntity u WHERE u.id = ?1")
	Set<UserEntity> findById2(long id);
	
	//@Query("SELECT u FROM UserEntity u WHERE u.firstName = 'Mattes'")
	//Collection<UserEntity> findAllMattesUsers();
}
