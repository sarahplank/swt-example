package de.eirikjapp.swt.services;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import de.eirikjapp.swt.entities.NewsletterContributionEntity;
import de.eirikjapp.swt.models.NewsletterContributionModel;
import de.eirikjapp.swt.models.NewsletterDTO;
import de.eirikjapp.swt.repositories.NewsletterCategoryRepository;
import de.eirikjapp.swt.repositories.NewsletterContributionRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class NewsletterContributionService {

	@Autowired
	private NewsletterContributionRepository newsletterContributionRepository;

    @Autowired
    private NewsletterCategoryRepository newsletterCategoryRepository;

	@Autowired
	private ModelMapper mapper;

	public NewsletterContributionModel findNewsletterContribution(long id) {
		Optional<NewsletterContributionEntity> optionalNewsletterContributionEntity = newsletterContributionRepository
				.findById(id);

		NewsletterContributionModel newsletterContributionModel = null;
		if (optionalNewsletterContributionEntity.isPresent()) {
			log.info("NewsletterContribution found: {}", optionalNewsletterContributionEntity.get().toString());
			newsletterContributionModel = mapper.map(optionalNewsletterContributionEntity.get(),
					NewsletterContributionModel.class);
		} else {
			log.error("NewsletterContribution not found for id: {}", id);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

		return newsletterContributionModel;
	}

	@Transactional // nur bei Schreiboperationen
	public NewsletterContributionModel createNewsletterContribution(NewsletterDTO newsletterDTO) {
		String date = newsletterDTO.getDateTime();
		LocalDateTime dateTime = LocalDateTime.parse(date);
		
		NewsletterContributionModel newsletterContributionModel = new NewsletterContributionModel();
		newsletterContributionModel.setLink(newsletterDTO.getLink());
		newsletterContributionModel.setLocationCategory(newsletterDTO.getLocationCategory());
		newsletterContributionModel.setNewsletterCategories(newsletterDTO.getNewsletterCategories());
		newsletterContributionModel.setText(newsletterDTO.getText());
		newsletterContributionModel.setTitle(newsletterDTO.getTitle());
		newsletterContributionModel.setUser(newsletterDTO.getUser());
		newsletterContributionModel.setDateTime(dateTime);
		
		NewsletterContributionEntity newsletterContributionEntity = mapper.map(newsletterContributionModel, NewsletterContributionEntity.class);
        
        newsletterContributionEntity = newsletterContributionRepository.save(newsletterContributionEntity);

        log.info("Newsletter Contribution created: {}", newsletterContributionEntity.toString());

        return mapper.map(newsletterContributionEntity, NewsletterContributionModel.class);
	}
}
