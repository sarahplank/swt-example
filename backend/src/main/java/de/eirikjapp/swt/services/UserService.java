package de.eirikjapp.swt.services;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import de.eirikjapp.swt.entities.UserEntity;
import de.eirikjapp.swt.models.ParentVerificationModel;
import de.eirikjapp.swt.models.Role;
import de.eirikjapp.swt.models.UserAnmeldungModel;
import de.eirikjapp.swt.models.UserModel;
import de.eirikjapp.swt.models.UserSearchModel;
import de.eirikjapp.swt.models.UserVerificationModel;
import de.eirikjapp.swt.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ModelMapper mapper;

	public Collection<UserModel> findUserByName(UserSearchModel user) {
		String firstName = user.getFirstName();
		String lastName = user.getLastName();
		Collection<UserEntity> userEntities = userRepository.findByName(firstName, lastName);
		Collection<UserModel> userModels = mapper.map(userEntities, new TypeToken<Collection<UserModel>>() {
		}.getType());
		return userModels; // map of collections
	}

	public UserModel findUser(long id) {
		Optional<UserEntity> optionalUserEntity = userRepository.findById(id);

		UserModel userModel = null;
		if (optionalUserEntity.isPresent()) {
			log.info("User found: {}", optionalUserEntity.get().toString());
			userModel = mapper.map(optionalUserEntity.get(), UserModel.class);
		} else {
			log.error("User not found for id: {}", id);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return userModel;
	}

	@Transactional // nur bei Schreiboperationen
	public UserModel createUser(UserModel userModel) {
		UserEntity userEntity = mapper.map(userModel, UserEntity.class);

		userEntity = userRepository.save(userEntity);

		log.info("User created: {}", userEntity.toString());

		return mapper.map(userEntity, UserModel.class);
	}

	public void addChildParent(long childId, long parentId) {
		UserEntity child = userRepository.findById(childId).get();
		UserEntity parent = userRepository.findById(parentId).get();

		child.getParents().add(parent);
		userRepository.save(child);

		// parent.getChilds().add(child);
		// userRepository.save(parent);

		log.info("Parent-Child connection created: childId={}, parentId={}", childId, parentId);
	}

	public UserVerificationModel verifyLogin(UserAnmeldungModel loginModel) {

		long id = loginModel.getId();
		String passwort = loginModel.getPasswort();

		Collection<UserEntity> userEntities = userRepository.findByIdAndPassword(id, passwort);

		UserVerificationModel verification = new UserVerificationModel();
		if (userEntities.size() == 1) {
			verification.setResult(true);
			verification.setId(userEntities.iterator().next().getId());
		} else {
			verification.setResult(false);
		}

		return verification;
	}

	public UserVerificationModel verifyParent(long parentId) {
		UserEntity userEntity = userRepository.findById(parentId).get();
		UserVerificationModel userVerificationModel = new UserVerificationModel();
		if(userEntity.getRole()==Role.UserOfFullAge || userEntity.getRole()==Role.Admin) {
			userVerificationModel.setResult(true);
			userVerificationModel.setId(parentId);
			return userVerificationModel;
		} else {
			userVerificationModel.setResult(false);
			userVerificationModel.setId(parentId);
			return userVerificationModel;
		}
		
	}
}
