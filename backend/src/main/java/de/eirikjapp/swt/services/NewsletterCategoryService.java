package de.eirikjapp.swt.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import de.eirikjapp.swt.entities.NewsletterCategoryEntity;
import de.eirikjapp.swt.models.AllNewsletterCategoryModel;
import de.eirikjapp.swt.models.NewsletterCategoryModel;
import de.eirikjapp.swt.repositories.NewsletterCategoryRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class NewsletterCategoryService {

    @Autowired
    private NewsletterCategoryRepository newsletterCategoryRepository;

    @Autowired
    private ModelMapper mapper;

    public NewsletterCategoryModel findNewsletterCategory(long id) {
        Optional<NewsletterCategoryEntity> optionalCategoryEntity = newsletterCategoryRepository.findById(id);
        
        NewsletterCategoryModel categoryModel = null;
        if (optionalCategoryEntity.isPresent()) {
            log.info("Category found: {}", optionalCategoryEntity.get().toString());
            categoryModel = mapper.map(optionalCategoryEntity.get(), NewsletterCategoryModel.class);
        } else {
            log.error("Category not found for id: {}", id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return categoryModel;
    }

    @Transactional //nur bei Schreiboperationen
    public NewsletterCategoryModel createNewsletterCategory(NewsletterCategoryModel newsletterCategoryModel) {
    	NewsletterCategoryEntity categoryEntity = mapper.map(newsletterCategoryModel, NewsletterCategoryEntity.class);
        
        categoryEntity = newsletterCategoryRepository.save(categoryEntity);

        log.info("Category created: {}", categoryEntity.toString());

        return mapper.map(categoryEntity, NewsletterCategoryModel.class);
    }
    
    public AllNewsletterCategoryModel getAllNewsletterCategories() {
    	AllNewsletterCategoryModel allNewsletterCategoryModels = new AllNewsletterCategoryModel();
    	List<NewsletterCategoryEntity> newsletterCategoryEntities = newsletterCategoryRepository.findAll();
    	List<NewsletterCategoryModel> newsletterCategoryModels = new ArrayList<>();
    	for(NewsletterCategoryEntity optionalNewsletterCategoryEntity : newsletterCategoryEntities) {
    		NewsletterCategoryModel newsletterCategoryModel = mapper.map(optionalNewsletterCategoryEntity, NewsletterCategoryModel.class);
    		newsletterCategoryModels.add(newsletterCategoryModel);
    	}
    	allNewsletterCategoryModels.setNewsletterCategoryModels(newsletterCategoryModels);
    	return allNewsletterCategoryModels;
    }
    
    public NewsletterCategoryModel getByName(String name) {
    	NewsletterCategoryEntity newsletterCategoryEntities = newsletterCategoryRepository.findByName(name);
    	NewsletterCategoryModel newsletterCategoryModel = mapper.map(newsletterCategoryEntities, NewsletterCategoryModel.class);
    	return newsletterCategoryModel;
    }
}
