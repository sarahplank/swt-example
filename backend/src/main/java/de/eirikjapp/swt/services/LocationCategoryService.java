package de.eirikjapp.swt.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import de.eirikjapp.swt.entities.LocationCategoryEntity;
import de.eirikjapp.swt.entities.NewsletterCategoryEntity;
import de.eirikjapp.swt.models.AllLocationCategoryModel;
import de.eirikjapp.swt.models.AllNewsletterCategoryModel;
import de.eirikjapp.swt.models.LocationCategoryModel;
import de.eirikjapp.swt.models.NewsletterCategoryModel;
import de.eirikjapp.swt.repositories.LocationCategoryRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class LocationCategoryService {

    @Autowired
    private LocationCategoryRepository locationCategoryRepository;

    @Autowired
    private ModelMapper mapper;

    public LocationCategoryModel findLocationCategory(long id) {
        Optional<LocationCategoryEntity> optionalLocationCategoryEntity = locationCategoryRepository.findById(id);

        LocationCategoryModel locationCategoryModel = null;
        if (optionalLocationCategoryEntity.isPresent()) {
            log.info("Category found: {}", optionalLocationCategoryEntity.get().toString());
            locationCategoryModel = mapper.map(optionalLocationCategoryEntity.get(), LocationCategoryModel.class);
        } else {
            log.error("Category not found for id: {}", id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        return locationCategoryModel;
    }

    @Transactional // nur bei Schreiboperationen
    public LocationCategoryModel createLocationCategory(LocationCategoryModel locationCategoryModel) {
        LocationCategoryEntity locationCategoryEntity = mapper.map(locationCategoryModel, LocationCategoryEntity.class);

        locationCategoryEntity = locationCategoryRepository.save(locationCategoryEntity);

        log.info("Category created: {}", locationCategoryEntity.toString());

        return mapper.map(locationCategoryEntity, LocationCategoryModel.class);
    }
    
    public AllLocationCategoryModel getAllLocationCategories() {
    	AllLocationCategoryModel allLocationCategoryModels = new AllLocationCategoryModel();
    	List<LocationCategoryEntity> locationCategoryEntities = locationCategoryRepository.findAll();
    	List<LocationCategoryModel> list = new ArrayList<>();
    	for(LocationCategoryEntity optionalLocationCategoryEntity : locationCategoryEntities) {
    		LocationCategoryModel locationCategoryModel = mapper.map(optionalLocationCategoryEntity, LocationCategoryModel.class);
    		list.add(locationCategoryModel);
    	}
    	allLocationCategoryModels.setLocationCategoryModels(list);
    	return allLocationCategoryModels;
    }
    
    public LocationCategoryModel getByName(String name) {
    	LocationCategoryEntity locationCategory = locationCategoryRepository.findByName(name);
    	return mapper.map(locationCategory, LocationCategoryModel.class);
    }
}
