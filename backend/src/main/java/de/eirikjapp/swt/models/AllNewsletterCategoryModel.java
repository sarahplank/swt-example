package de.eirikjapp.swt.models;

import java.util.List;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class AllNewsletterCategoryModel {
	List<NewsletterCategoryModel> newsletterCategoryModels;
}
