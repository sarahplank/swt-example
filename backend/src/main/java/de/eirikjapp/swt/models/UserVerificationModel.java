package de.eirikjapp.swt.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class UserVerificationModel {
    private Boolean result;

    private Long id;
}
