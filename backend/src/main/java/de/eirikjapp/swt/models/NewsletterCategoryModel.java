package de.eirikjapp.swt.models;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class NewsletterCategoryModel {
	
	private Long id;
	
	private String categoryName;
	
	private List<NewsletterContributionModel> newsletterContributionModels;
}
