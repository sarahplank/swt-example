package de.eirikjapp.swt.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "location_categories")
public class LocationCategoryEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;

    @Column(name = "location", unique = true)
    private String location;

    @OneToMany(mappedBy = "locationCategory")
    private List<NewsletterContributionEntity> newsletterContributionEntities;

    // private List<NewsletterContributionEntity> newsletterContributionEntities;

    /*
     * @ManyToOne
     * 
     * @JoinColumn(name="newsletterContribution_id", nullable=false) private
     * NewsletterContributionEntity newsletterContribution;
     */
}