package de.eirikjapp.swt.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "newsletter_categories")
public class NewsletterCategoryEntity implements Serializable {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true)
	private Long id;

    @Column(name = "name", unique = true)
    private String name;

    @ManyToMany(mappedBy = "newsletterCategories")
    Set<NewsletterContributionEntity> newsletterContributions = new HashSet<NewsletterContributionEntity>();
	

	// private List<NewsletterContributionEntity> newsletterContributionEntities;

	/*
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="newsletterContribution_id", nullable=false) private
	 * NewsletterContributionEntity newsletterContribution;
	 */
}
