package de.eirikjapp.swt.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import de.eirikjapp.swt.models.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class UserEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true)
	private Long id;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "lastName")
	private String lastName;

	@Column(name = "passwort")
	private String passwort;

	@Column(name = "email")
	private String email;
	
	@Column(name = "profilePictureLink")
	private String profilePictureLink;
	
	@Column(name = "club")
	private String club;

	@OneToMany(mappedBy = "user")
	private List<NewsletterContributionEntity> newsletterContributionEntities;

	/*
	@OneToOne(mappedBy = "user")
	@PrimaryKeyJoinColumn
	@NotFound(action = NotFoundAction.IGNORE)
	private UserOfFullAgeEntity userOfFullAge;

	@OneToOne(mappedBy = "user")
	@PrimaryKeyJoinColumn
	@NotFound(action = NotFoundAction.IGNORE)
	private ChildEntity child; */
	
	
	@Column(name = "role")
	private Role role;
	
	@ElementCollection
	@Column(name = "parent_id")
	private List<UserEntity> parents;
	
	/*
	@ElementCollection
	@Column(name = "child_id")
	private List<UserEntity> childs; */
}

