package de.eirikjapp.swt.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import de.eirikjapp.swt.models.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "newsletter_contributions")
public class NewsletterContributionEntity implements Serializable{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;

    @Column(name = "title")
    private String title;
    
    @Column(name = "text")
    private String text;
    
    @Column(name = "link")
    private String link;
    
    @ManyToOne
    @JoinColumn(name = "locationCategory_id", nullable = false)
    private LocationCategoryEntity locationCategory;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;
    
    @Column(name = "DateAndTimeOfCreation")
    private LocalDateTime dateTime;
    
    /*
    @OneToMany(mappedBy="newsletterContribution")
    private List<CategoryEntity> categoryEntities; */
    @ManyToMany
    @JoinTable(joinColumns = @JoinColumn(name = "newsletter_contributions_id"), inverseJoinColumns = @JoinColumn(name = "newsletter_categories_id"))
    private Set<NewsletterCategoryEntity> newsletterCategories = new HashSet<NewsletterCategoryEntity>();
    
    /*@ManyToOne
    @JoinColumn(name="category_id1", nullable=false)
    private CategoryEntity category;
    */
}


