package de.eirikjapp.swt.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import de.eirikjapp.swt.models.AllLocationCategoryModel;
import de.eirikjapp.swt.models.AllNewsletterCategoryModel;
import de.eirikjapp.swt.models.LocationCategoryModel;
import de.eirikjapp.swt.models.NewsletterCategoryModel;

@RestController
@RequestMapping("locationCategories")
public interface LocationCategoryController {

    @GetMapping("/{id}")
    @ResponseBody
    public LocationCategoryModel get(@PathVariable long id);

    @PutMapping
    public ResponseEntity<LocationCategoryModel> put(@RequestBody LocationCategoryModel locationCategoryModel);
    
    @GetMapping("/all")
    public AllLocationCategoryModel getAll();
    
    @GetMapping("/findByName/{name}")
    public LocationCategoryModel getByName(@PathVariable String name);
}

