package de.eirikjapp.swt.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import de.eirikjapp.swt.models.AllLocationCategoryModel;
import de.eirikjapp.swt.models.AllNewsletterCategoryModel;
import de.eirikjapp.swt.models.LocationCategoryModel;
import de.eirikjapp.swt.models.NewsletterCategoryModel;
import de.eirikjapp.swt.services.LocationCategoryService;
import de.eirikjapp.swt.services.NewsletterCategoryService;

@RestController
public class LocationCategoryControllerImpl implements LocationCategoryController {

    @Autowired
    private LocationCategoryService locationCategoryService;

    @Override
    public LocationCategoryModel get(long id) {
        return locationCategoryService.findLocationCategory(id);
    }
    
    @Override
    public ResponseEntity<LocationCategoryModel> put(LocationCategoryModel locationCategory) {
        LocationCategoryModel createdCategory = locationCategoryService.createLocationCategory(locationCategory);
        return new ResponseEntity<>(createdCategory, HttpStatus.CREATED);
    }
    
    @Override
    public AllLocationCategoryModel getAll() {
    	AllLocationCategoryModel allLocationCategoryModel = locationCategoryService.getAllLocationCategories();
    	return allLocationCategoryModel;
    }
    
    @Override
    public LocationCategoryModel getByName(String name) {
    	return locationCategoryService.getByName(name);
    }
}