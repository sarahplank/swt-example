package de.eirikjapp.swt.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import de.eirikjapp.swt.models.AllNewsletterCategoryModel;
import de.eirikjapp.swt.models.NewsletterCategoryModel;
import de.eirikjapp.swt.services.NewsletterCategoryService;

@RestController
public class NewsletterCategoryControllerImpl implements NewsletterCategoryController {

    @Autowired
    private NewsletterCategoryService newsletterCategoryService;

    @Override
    public NewsletterCategoryModel get(long id) {
        return newsletterCategoryService.findNewsletterCategory(id);
    }
    
    @Override
    public ResponseEntity<NewsletterCategoryModel> put(NewsletterCategoryModel newsletterCategory) {
        NewsletterCategoryModel createdCategory = newsletterCategoryService
                .createNewsletterCategory(newsletterCategory);
        return new ResponseEntity<>(createdCategory, HttpStatus.CREATED);
    }
    
    @Override
    public AllNewsletterCategoryModel getAll() {
    	AllNewsletterCategoryModel allNewsletterCategoryModel = newsletterCategoryService.getAllNewsletterCategories();
    	return allNewsletterCategoryModel;
    }
    
    @Override
    public NewsletterCategoryModel getByName(String name) {
    	NewsletterCategoryModel newsletterCategoryModel = newsletterCategoryService.getByName(name);
    	return newsletterCategoryModel;
    }
}