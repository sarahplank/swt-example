package de.eirikjapp.swt.controllers;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import de.eirikjapp.swt.models.NewsletterContributionModel;
import de.eirikjapp.swt.models.NewsletterDTO;

@RestController
@RequestMapping("newsletterContributions")
public interface NewsletterContributionController {

    @GetMapping("/{id}")
    @ResponseBody
    public NewsletterContributionModel get(@PathVariable long id);

    @PutMapping
    public ResponseEntity<NewsletterContributionModel> put(@RequestBody NewsletterDTO newsletterDTO);
}