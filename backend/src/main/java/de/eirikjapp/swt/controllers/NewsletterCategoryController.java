package de.eirikjapp.swt.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import de.eirikjapp.swt.models.AllNewsletterCategoryModel;
import de.eirikjapp.swt.models.NewsletterCategoryModel;

@RestController
@RequestMapping("newsletterCategories")
public interface NewsletterCategoryController {

    @GetMapping("/{id}")
    @ResponseBody
    public NewsletterCategoryModel get(@PathVariable long id);

    @PutMapping
    public ResponseEntity<NewsletterCategoryModel> put(@RequestBody NewsletterCategoryModel user);
    
    @GetMapping("/all")
    public AllNewsletterCategoryModel getAll();
    
    @GetMapping("/findByName/{name}")
    public NewsletterCategoryModel getByName(@PathVariable String name);
}
