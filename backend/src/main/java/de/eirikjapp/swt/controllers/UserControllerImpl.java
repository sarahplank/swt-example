package de.eirikjapp.swt.controllers;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import de.eirikjapp.swt.models.ParentVerificationModel;
import de.eirikjapp.swt.models.UserAnmeldungModel;
import de.eirikjapp.swt.models.UserModel;
import de.eirikjapp.swt.models.UserSearchModel;
import de.eirikjapp.swt.models.UserVerificationModel;
import de.eirikjapp.swt.services.UserService;

@RestController
public class UserControllerImpl implements UserController {

    @Autowired
    private UserService userService;

    @Override
    public UserModel get(long id) {
        return userService.findUser(id);
    }

    @Override
    public ResponseEntity<UserModel> put(UserModel user) {
        UserModel createdUser = userService.createUser(user);
        return new ResponseEntity<>(createdUser, HttpStatus.CREATED);
    }
    
    @Override
    public Collection<UserModel> findByName(UserSearchModel user) {
    	return userService.findUserByName(user);
    }
    
    @Override
    public void addConnectionChildParent(long childId, long parentId) {
    	userService.addChildParent(childId, parentId);
    }
    
    @Override
    public UserVerificationModel verifyLogin(UserAnmeldungModel loginModel, HttpServletRequest request,
            HttpSession session) {

        session.invalidate();
        HttpSession newSession = request.getSession();
        
    	UserVerificationModel userVerificationModel = userService.verifyLogin(loginModel);
        newSession.setAttribute("USER", userVerificationModel.getId());

    	return userVerificationModel;
    }

    @Override
    public void logout(HttpSession session) {
        session.invalidate();
    }
    
    @Override
    public UserVerificationModel verifyParent(long parentId) {
    	return userService.verifyParent(parentId);
    }
}
