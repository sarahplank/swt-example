package de.eirikjapp.swt.controllers;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import de.eirikjapp.swt.models.ParentVerificationModel;
import de.eirikjapp.swt.models.UserAnmeldungModel;
import de.eirikjapp.swt.models.UserModel;
import de.eirikjapp.swt.models.UserSearchModel;
import de.eirikjapp.swt.models.UserVerificationModel;

@RestController
@RequestMapping("users")
public interface UserController {

    @GetMapping("/{id}")
    @ResponseBody
    public UserModel get(@PathVariable long id);

    @PutMapping
    public ResponseEntity<UserModel> put(@RequestBody UserModel user);
    
    @PostMapping("/search")
    @ResponseBody
    public Collection<UserModel> findByName(@RequestBody UserSearchModel user);
    
    @PutMapping("/{childId}/addParent/{parentId}")
    @ResponseBody
    public void addConnectionChildParent(@PathVariable long childId, @PathVariable long parentId);
    
    @PostMapping("/verifyLogin")
    public UserVerificationModel verifyLogin(@RequestBody UserAnmeldungModel loginModel, HttpServletRequest request,
            HttpSession session);

    @GetMapping("/logout")
    @ResponseBody
    public void logout(HttpSession session);
    
    @GetMapping("/verifyParent/{parentId}")
    @ResponseBody
    public UserVerificationModel verifyParent(@PathVariable long parentId);
}
