package de.eirikjapp.swt.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import de.eirikjapp.swt.models.NewsletterContributionModel;
import de.eirikjapp.swt.models.NewsletterDTO;
import de.eirikjapp.swt.services.NewsletterContributionService;

@RestController
public class NewsletterContributionControllerImpl implements NewsletterContributionController {

    @Autowired
    private NewsletterContributionService newsletterContributionService;

    @Override
    public NewsletterContributionModel get(long id) {
        return newsletterContributionService.findNewsletterContribution(id);
    }

    @Override
    public ResponseEntity<NewsletterContributionModel> put(NewsletterDTO newsletterDTO) {
    	NewsletterContributionModel createdNewsletterContribution = newsletterContributionService.createNewsletterContribution(newsletterDTO);
        return new ResponseEntity<>(createdNewsletterContribution, HttpStatus.CREATED);
    }
}
