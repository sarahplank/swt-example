package de.eirikjapp.swt;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.GetMapping;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    @GetMapping("/")
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SwtApplication.class);
    }

}
